import { useDispatch } from "react-redux";
import { Dispatch } from "redux";
export default function useToast() {
  const dispatch = useDispatch<Dispatch<IToastAction>>();
  return (type: AlertColor, msg: string) => {
    dispatch({
      type: "SHOW_TOAST",
      payload: {
        toastMessage: msg,
        toastType: type,
      },
    });
  };
}
// 还可以继续简化
export function useToastSuccess() {
  const taost = useToast();
  return (msg: string) => taost("success", msg);
}
export function useToastError() {
  const taost = useToast();
  return (msg: string) => taost("error", msg);
}
