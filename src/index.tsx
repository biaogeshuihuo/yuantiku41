import React from "react";
import ReactDOM from "react-dom/client";
import "@/assets/style/common.less";
import router from "./router";
import { Provider } from "react-redux";
import store from "./store";
import Toast from "./components/Toast";
const div = document.getElementById("root")!; // !代表div一定存在
const root = ReactDOM.createRoot(div);
root.render(
  <Provider store={store}>
    {router}
    {/* 因为toast组件是任何页面都可能用到的，我们直接放到根标签下面 */}
    <Toast />
  </Provider>
);
