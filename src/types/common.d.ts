interface KeyMap {
  key: string;
  value: string;
}

interface StringMap {
  // ts里面需要给一个索引器才能让对象可以直接使用 [key] 的方式访问数据
  [index: string]: string;
}
