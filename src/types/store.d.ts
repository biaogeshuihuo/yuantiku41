// xxx.d.ts 这种文件，是用来全局声明的，只要在这里面声明的数据，在别的地方不用引入，直接就可以使用
// 声明一个表示 Store 里面的 state 的 interface
// 因为 state 这个数据，是在任何一个组件中都可能用到的，就需要使用到它所属的类型

type AlertColor = "success" | "info" | "warning" | "error";

interface IRootState {
  toastData: IToastData;
}

// 声明一个 Store 里面要用到的action
interface IRootAction {
  type: string;
  payload: any;
}

interface IToastPayload {
  toastMessage: string;
  toastType: AlertColor;
}

interface IToastData extends IToastPayload {
  // 是否显示toast
  toastShow: boolean;
}

// 定义一个用来让 toast 显示的action
interface IToastAction {
  type: string;
  payload: IToastPayload;
}
