// 声明一个用于向服务器发请求的登录的参数
interface ILoginParams {
  username: string;
  password: string;
}

// 定义一个公共的返回的格式
interface IResponse<T> {
  errCode: number;
  message: string;
  data: T;
}

//Prmiose<IResponse<T>> 会大量地重复使用，显示得比较麻烦，需要简化一个
type IPR<T> = Promise<IResponse<T>>;

interface IExamItem {
  actionCode: string;
  actionName: string;
  actionType: string;
  businessLevel: number;
  createTime: string;
  createUser: string;
  id: number;
  info: string;
  invalid: number;
  itemCount: number;
  numberLevel: number;
  pid: number;
  sort: number;
  tenantId: number;
  title: string;
  updateTime: string;
}

interface IHomeData {
  collect: number;
  study: number;
  wrong: number;
  errCode: number;
  message: string;
  exam: IExamItem;
  exemItems: IExamItem[];
}

// 请求回来的题目分类的数据
interface IQuestionTypeKeyMap {
  key: string;
  value: number;
  sort: number;
}

// 请求练习题目数组的参数
interface IQuestionParams {
  testNum: number;
  testType: string;
  actionCode: string;
  questionType: string;
}

interface IQuestionItem {
  analysis: string;
  answer: string;
  categoryCode: string;
  collcetStatus: boolean;
  collcetTotal: number;
  content: string;
  id: number;
  invalid: number;
  oerationTotal: number;
  optionContent: string;
  questionType: string;
  score: number;
  sort: number;
  tenantId: string;
  title: string;
  userAnswer: string;
  wrongAnswer: string;
}

interface SideSubjectsData {
  id: number;
  title: string;
}
