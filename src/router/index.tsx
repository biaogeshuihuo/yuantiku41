import App from "@/views/App";
// import Fast from "@/views/Fast";
// import Home from "@/views/Home";
import Login from "@/views/Login";
import React, { lazy } from "react";
// import Mine from "@/views/Mine";
// import Practise from "@/views/Practise";
// import Select from "@/views/Select";
// import Subjects from "@/views/Subjects";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";

// react里面路由的懒加载是 通过一个 React.lazy() 方法来实现
const Fast = lazy(() => import("@/views/Fast"));
const Mine = lazy(() => import("@/views/Mine"));
const Practise = lazy(() => import("@/views/Practise"));
const Select = lazy(() => import("@/views/Select"));
const Subjects = lazy(() => import("@/views/Subjects"));
const Home = lazy(() => import("@/views/Home"));

const router = (
  <Router>
    <Routes>
      <Route path="/login" element={<Login />}></Route>
      {/* 因为除了登录注册两个页面之外，所有的其它页面都会登录后才能访问 */}
      {/* 我们需要在一个父组件里面验证是否有登录，所以我们准备一个1级路由，来验证是否登录，所有需要登录的子组件，都放在这个1级路由的下面 */}
      <Route path="/" element={<App />}>
        <Route path="home" element={<Home />}></Route>
        <Route path="fast" element={<Fast />}></Route>
        <Route path="mine" element={<Mine />}></Route>
        <Route path="select/:actionCode" element={<Select />}></Route>
        <Route path="practise" element={<Practise />}></Route>
        <Route path="subjects" element={<Subjects />}></Route>
      </Route>
    </Routes>
  </Router>
);

export default router;
