import { sideSubjectsDataApi } from "@/api";
import React, { useState } from "react";
import "./subjects.less";
import style from "./example.module.css";
// console.log(style);
import lessStyle from "./example.module.less";
console.log(lessStyle);

export default function Subjects() {
  const [subjects, setSubjects] = useState<SideSubjectsData[]>([]);
  // 目标： 在点击科目的时候，不需要每次都发请求，而有选择的发
  // 策略： 第一次请求，就发
  //      然后把数据存到 sessionStorage
  // 额外的需求
  // 顺便存上一次请求的时间，当下一次点击超过多久，再次重新请求
  const getSubjects = () => {
    // 先判断是否已经有数据，如果有，直接从缓存里面取
    let data = sessionStorage.getItem("subjects");
    if (data) {
      setSubjects(JSON.parse(data));
    } else {
      sideSubjectsDataApi().then((res) => {
        // 把数据存到sessionStorage
        if (res.errCode === 0) {
          sessionStorage.setItem("subjects", JSON.stringify(res.data));
          setSubjects(res.data);
        }
      });
    }
  };

  return (
    <div className="subjects">
      <section className="sub" onClick={getSubjects}>
        科目
      </section>

      <div>
        <ul>
          {subjects.map((item) => (
            <li key={item.id}>{item.title}</li>
          ))}
        </ul>
      </div>
      {/* 这个用法叫： css in js */}
      <div className={style.item}>123</div>
      <div className={lessStyle.subjects}>
        <div className={lessStyle.example}>hello</div>
      </div>
    </div>
  );
}
