import React, { useState } from "react";
import "./selectgrid.less";

interface IProp {
  title: string;
  list: KeyMap[];
  onChange: (value: string) => void;
}

export default function SelectGrid({ title, list, onChange }: IProp) {
  // 当前是哪个选中
  const [curIndex, setCurIndex] = useState(0);
  const clickHandle = (index: number) => {
    setCurIndex(index);
    // 调用父组件传递进来的onChanage 把 对应的value 传递给父组件
    onChange(list[index].value);
  };

  return (
    <div className="sel-grid">
      <h4>{title}</h4>
      <ul>
        {list.map((item, index) => (
          <li
            onClick={() => clickHandle(index)}
            key={item.value}
            className={index === curIndex ? "active" : ""}
          >
            {item.key}
          </li>
        ))}
      </ul>
    </div>
  );
}
