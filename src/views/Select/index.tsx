import { questionTypeDataApi } from "@/api";
import { Button } from "@mui/material";
import React, { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import SelectGrid from "./components/SelectGrid";
import "./select.less";

// 试题分类数组
const TestTypes: KeyMap[] = [
  { key: "全部", value: "all" },
  { key: "已做", value: "done" },
  { key: "未做", value: "notdone" },
  { key: "错误", value: "err" },
];

// 题目数量数组
const TestNums: KeyMap[] = [
  { key: "5", value: "5" },
  { key: "10", value: "10" },
  { key: "20", value: "20" },
  { key: "30", value: "30" },
  { key: "50", value: "50" },
  { key: "100", value: "100" },
];

const questionTypeMaps: StringMap = {
  all: "全部",
  qa: "问答",
  fill: "填空",
  one: "单选",
  check: "判断",
  code: "编程",
  many: "多选",
};

export default function Select() {
  // 获取actionCode
  const { actionCode } = useParams();

  const [questionTypes, setQuestionTypes] = useState<KeyMap[]>([]);
  // 进入当前页面的时候，需要根据 actionCode 去服务器请求对应的题目类型
  useEffect(() => {
    questionTypeDataApi(actionCode).then((res) => {
      if (res.errCode === 0) {
        // const temp: KeyMap[] = [];
        // res.data.forEach((item) => {
        //   temp.push({
        //     key: questionTypeMaps[item.key] + item.value,
        //     value: item.key,
        //   });
        // });
        const temp = res.data.map((item) => ({
          key: questionTypeMaps[item.key] + item.value,
          value: item.key,
        }));
        setQuestionTypes(temp);
      }
    });
  }, []);
  // // 获取试题分类
  // const testTypeChange = (value: string) => {
  // };
  // // 获取试题数量
  // const testNumChange = (value: string) => {
  // };

  const navigate = useNavigate();
  const [testType, setTestType] = useState("all");
  const [questionType, setQuestionType] = useState("all");
  const [testNum, setTestNum] = useState(5);

  return (
    <div className="select">
      <div className="btns">
        <Button
          variant="contained"
          onClick={() =>
            navigate("/practise", {
              state: { actionCode, testNum, testType, questionType },
            })
          }
        >
          进入练习模式
        </Button>
        <Button variant="contained" color="warning">
          进入考试模式
        </Button>
      </div>
      <SelectGrid
        title="试题分类"
        list={TestTypes}
        onChange={(value) => setTestType(value)}
      />
      <SelectGrid
        title="题目分类"
        list={questionTypes}
        onChange={(value) => setQuestionType(value)}
      />
      <SelectGrid
        title="做题数量"
        list={TestNums}
        onChange={(value) => setTestNum(parseInt(value))}
      />
    </div>
  );
}
