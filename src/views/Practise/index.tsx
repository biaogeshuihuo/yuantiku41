import { getQuestionListApi } from "@/api";
import React, { useEffect, useState, TouchEvent } from "react";
import { useLocation } from "react-router-dom";
import Choice from "./components/Choice";
import Fill from "./components/Fill";
import "./practise.less";
import ArticleIcon from "@mui/icons-material/Article";
import StarBorderRoundedIcon from "@mui/icons-material/StarBorderRounded";
import { Button } from "@mui/material";
import { Swiper, SwiperSlide } from "swiper/react";
import "swiper/css";
const ChoiceType = ["one", "many", "check"];

export default function Practise() {
  const location = useLocation();
  const state = location.state as IQuestionParams;

  const [list, setList] = useState<IQuestionItem[]>([]);
  useEffect(() => {
    getQuestionListApi(state).then((res) => {
      if (res.errCode === 0) {
        setList(res.data);
      }
    });
  }, []);

  // 当前是第几个题目
  const [curIndex, setCurIndex] = useState(0);
  // 移动端的事件 一般选择使用 touch 事件
  // touchstart touchmove  touchend
  // 当手指松开，判断向左还是向右，切换题目，修改 cona 的位置
  // let startX = 0;
  // const start = (e: TouchEvent<HTMLDivElement>) => {
  //   startX = e.touches[0].clientX;
  // };

  // const end = (e: TouchEvent<HTMLDivElement>) => {
  //   // 在松手的时候，只能人这个属性去获取坐标点
  //   let endX = e.changedTouches[0].clientX;
  //   // 当我手指要移动超过一定的距离才算是滑动
  //   if (Math.abs(endX - startX) < 50) return;
  //   if (endX > startX) {
  //     // 左 -> 右
  //     setCurIndex(curIndex - 1);
  //   } else {
  //     setCurIndex(curIndex + 1);
  //   }
  // };
  return (
    <div className="practise">
      <header>
        <ArticleIcon />
        <span className="calc">
          {curIndex + 1}/{list.length}
        </span>
        <Button>
          <span>收藏</span>
          <StarBorderRoundedIcon />
        </Button>
      </header>
      {/* 所有的题目分成两个大类 ： 选择 / 填空 */}
      {/* 遍历所有的题目，根据不同的类型展示不同组件 */}
      {/* <div className="list-box" onTouchStart={start} onTouchEnd={end}>
        <div
          className="cona"
          style={{
            width: 100 * list.length + "vw",
            transform: `translate(-${100 * curIndex}vw)`,
          }}
        >
          {list.map((item) => {
            if (ChoiceType.includes(item.questionType)) {
              return <Choice data={item} key={item.id} />;
            } else {
              return <Fill data={item} key={item.id} />;
            }
          })}
        </div>
      </div> */}

      {/* 使用swiper实现切换 */}
      <Swiper onSlideChange={(swiper) => setCurIndex(swiper.activeIndex)}>
        {list.map((item) => (
          <SwiperSlide key={item.id}>
            {ChoiceType.includes(item.questionType) ? (
              <Choice data={item} />
            ) : (
              <Fill data={item} />
            )}
          </SwiperSlide>
        ))}
      </Swiper>
    </div>
  );
}
