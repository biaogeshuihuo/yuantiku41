import { Checkbox } from "@mui/material";
import Brightness1OutlinedIcon from "@mui/icons-material/Brightness1Outlined";
import CheckCircleIcon from "@mui/icons-material/CheckCircle";
import React, { useState } from "react";
import "./choiceitem.less";
interface IProp {
  data: IQuestionTypeKeyMap;
  onChange: (value: string) => void;
  answers: string[];
}

export default function ChoiceItem({ data, onChange, answers }: IProp) {
  // 当前组件内部是否勾选不是自己决定的，而由父组件里面记录了用户最后选择的答案，由这个用户选择的答案决定内部是否勾选
  const checkHandle = () => {
    onChange(data.key);
  };
  return (
    <div className="choice-item" onClick={checkHandle}>
      <div className="check">
        <Checkbox
          checked={answers.includes(data.key)}
          icon={<Brightness1OutlinedIcon />}
          checkedIcon={<CheckCircleIcon />}
        ></Checkbox>
      </div>
      <div className="option">{data.key}</div>
      <div className="value">{data.value}</div>
    </div>
  );
}
