import { Button, TextField } from "@mui/material";
import React, { useState } from "react";
import "./fill.less";
interface IProp {
  data: IQuestionItem;
}

const typeMap: StringMap = {
  fill: "填空",
  code: "编程",
  qa: "问答",
};
// 填空题型的组件
export default function Fill({ data }: IProp) {
  const [text, setText] = useState("");

  const chnageHanle = (
    e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>
  ) => {
    setText(e.target.value);
  };
  return (
    <div className="fill">
      <section className="detail">
        <span className="type">{typeMap[data.questionType]}</span>
        <div className="title">{data.title}</div>
      </section>
      {/* dangerouslySetInnerHTML 类似 v-html 用来渲染富文本的效果 */}
      <section
        className="detail-content"
        dangerouslySetInnerHTML={{ __html: data.content }}
      ></section>
      <section>
        <TextField
          value={text}
          onChange={chnageHanle}
          fullWidth
          multiline={true}
          rows={5}
          placeholder="请输入答案"
        />
      </section>
      <section>
        <Button variant="outlined">确定</Button>
      </section>
    </div>
  );
}
