import { Button, LinearProgress } from "@mui/material";
import React from "react";
import { useNavigate } from "react-router-dom";
import "./listitem.less";
// import from 的方式引入图片，会在一开始 build 的时候就打包到 bunddle.js 里面，会在一开始加载的时候就请求到浏览器里面了
// import src from "@/assets/images/home/subject.png";
// require的方式则会在打包的时候独立出来，在页面上使用到这个图片的时候，才会重新请求加载
const src = require("@/assets/images/home/subject.png");
// 当图片不是第一次加载页面就需要加载时，建议使用require
// 当然如果第一次加载的页面如果图片太多，也可以只把首屏的图片使用import导入，其它使用require导入
// 以上规则只对静态图片有效

// 每个组件当进行父传子的时候，需要定义props的类型
interface IProp {
  data: IExamItem;
}

export default function ListItem({ data }: IProp) {
  const navigate = useNavigate();
  const goto = () => {
    navigate("/select/" + data.actionCode);
  };
  return (
    <div className="item">
      <img src={src} alt="" />
      <section className="m">
        <h5>{data.title}</h5>
        <p>1/{data.itemCount}题</p>
        <LinearProgress variant="determinate" value={50} />
      </section>
      <Button onClick={goto} variant="contained">
        练习
      </Button>
    </div>
  );
}
