import { Button } from "@mui/material";
import React, { useEffect, useState } from "react";
import ArrowRightIcon from "@mui/icons-material/ArrowRight";
import "./home.less";
import { HomeDataApi } from "@/api";
import ListItem from "./components/ListItem";
import { useNavigate } from "react-router-dom";

export default function Home() {
  const [title, setTitle] = useState("");
  const [study, setStudy] = useState(0);
  const [total, setTotal] = useState(0);
  const [wrong, setWrong] = useState(0);
  const [collect, setCollect] = useState(0);

  // 请求回来的数组
  const [list, setList] = useState<IExamItem[]>([]);
  useEffect(() => {
    HomeDataApi().then((res) => {
      if (res.errCode === 0) {
        // 把数据渲染出来
        setTitle(res.data.exam.title);
        setStudy(res.data.study);
        setTotal(res.data.exam.itemCount);
        setWrong(res.data.wrong);
        setCollect(res.data.collect);
        setList(res.data.exemItems);
      }
    });
  }, []);
  const navigate = useNavigate();
  return (
    <div className="home">
      <section className="subject-name">
        <h3>{title}</h3>
        <Button security="type" onClick={() => navigate("/subjects")}>
          <span>切换考试科目</span>
          <ArrowRightIcon />
        </Button>
      </section>
      <section className="welcom">
        <section className="img"></section>
        <section className="total">
          <ul>
            <li>已学{study}题</li>
            <li>共{total}题</li>
          </ul>
          <ul>
            <li>
              <p>{wrong}</p>
              <p>错题</p>
            </li>
            <li>
              <p>{collect}</p>
              <p>收藏</p>
            </li>
          </ul>
        </section>
      </section>
      <section className="tiku">
        <h4>学科题库</h4>
        {list.map((item) => (
          <ListItem data={item} key={item.id} />
        ))}
      </section>
    </div>
  );
}
