import BackPrev from "@/components/BackPrev";
import BottomNav from "@/components/BottomNav";
import React, { Suspense, useEffect } from "react";
import { Outlet, useNavigate } from "react-router-dom";
import "@/assets/style/app.less";
import { useToastError } from "@/hooks/useToast";
import EventBus from "@/utils/EventBus";
import Loading from "@/components/Loading";

export default function App() {
  const error = useToastError();
  const navigate = useNavigate();
  // 在App组件里面进行登录的拦截
  // 在构建 路由的时候， 特意把所有需要登录后才能操作的组件，放到 App的子路由(子组件)里面
  // 只要使用到 App作为父路由的子路由，都会经过这里
  // 判断是否登录
  let token = localStorage.getItem("token");
  useEffect(() => {
    if (!token) {
      // 弹出提示
      error("请先登录");
      // 跳转到 login
      navigate("/login");
    }
  });

  useEffect(() => {
    if (token) {
      navigate("/home");
    }
    EventBus.on("error", (msg: string) => {
      error(msg);
    });

    EventBus.on("login_timeout", () => {
      // 1. 提示
      error("登录失效，请重新登录");
      // 3. 清空token
      localStorage.removeItem("token");
      // 2. 跳转
      navigate("/login");
    });
  }, []);

  // 条件渲染
  return token ? (
    <>
      {/* Suspense 这个组件是用来给 进行懒加载的提示用的 */}
      <Suspense fallback={<Loading />}>
        <Outlet />
      </Suspense>
      <BottomNav />
      <BackPrev />
    </>
  ) : (
    <></>
  );
}
