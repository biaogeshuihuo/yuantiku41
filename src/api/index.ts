import request from "./request";

// export const LoginApi = (params:ILoginParams):Promise<IResponse<string>>=>request.post('/1024/login',params)
export const LoginApi = (params: ILoginParams): IPR<string> =>
  request.post("/1024/login", params);

export const HomeDataApi = (): IPR<IHomeData> => request.get("/6666");

// 根据actionCode去服务器获取 题目分类数据
export const questionTypeDataApi = (
  actionCode: string | undefined
): IPR<IQuestionTypeKeyMap[]> => request.get(`/1314/${actionCode}/all`);

// 根据选择的题目的数据，发请求得到练习的题目列表
export const getQuestionListApi = (
  params: IQuestionParams
): IPR<IQuestionItem[]> => request.post("/1314", params);

// 获取所有的学科名称
export const sideSubjectsDataApi = (): IPR<SideSubjectsData[]> =>
  request.get("/6666/field");
