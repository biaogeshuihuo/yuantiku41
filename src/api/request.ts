// 封装一个axios实例，对外导出
import EventBus from "@/utils/EventBus";
import axios, { AxiosError } from "axios";

const request = axios.create({
  baseURL: "/api",
  timeout: 5000,
});

request.interceptors.request.use(
  (config) => {
    // 几乎所有的接口，都要 token，放在请求头里面
    let token = localStorage.getItem("token");
    if (token) {
      config.headers = config.headers || {};
      config.headers["x-auth-token"] = token;
    }
    return config;
  },
  (err) => Promise.reject(err)
);

// 响应拦截里面可以得到响应回来的数据
// 有两个回调函数，第一个回调函数是当响应状态码是 200 进去的
// 第二个回调函数，是响应状态码是非 200 进去
request.interceptors.response.use(
  (res) => {
    return res.data;
  },
  (err: AxiosError<IResponse<null>>) => {
    // 现在的接口服务器，每当我们的请求有问题，都是响应回来非200
    // 把所有的不成功的请求，都在这进行处理
    // console.log(err);
    // alert(err.response.data.message);
    // 如果是登录已经失效
    // 目前这个项目使用errCode 是 1002 代表登录失效
    if (err.response?.data.errCode === 1002) {
      // 使用订阅发布实现登录失效
      EventBus.emit("login_timeout");
    } else {
      EventBus.emit("error", err.response?.data.message);
    }

    Promise.reject(err);
  }
);

export default request;
