/**
 *  封装一个订阅发布模式，用于解决  不能在  拦截器里面使用 hook 的问题
 *
 */

interface IEvents {
  [index: string]: Function[];
}

interface IEventBus {
  on: Function;
  emit: Function;
  events: IEvents;
}

const EventBus: IEventBus = {
  events: {},
  // on方法用来订阅的
  on(key: string, callback: Function) {
    // 判断对象身上是否有与key相关的数组，如果有就把函数push进去
    if (!this.events[key]) {
      this.events[key] = [];
    }
    this.events[key].push(callback);
  },
  //   emit方法用来发布
  emit(key: string, message: string) {
    // 把所有和key相关的函数全调用
    if (this.events[key]) {
      this.events[key].forEach((fn) => fn(message));
    }
  },
};

export default EventBus;
