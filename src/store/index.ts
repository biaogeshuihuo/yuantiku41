import { createStore } from "redux";

const defState: IRootState = {
  toastData: {
    toastShow: false,
    toastMessage: "test",
    toastType: "error",
  },
};

const reducer = (state: IRootState = defState, action: IRootAction) => {
  state = JSON.parse(JSON.stringify(state));
  // 根据 action.type 进行判断
  switch (action.type) {
    // 显示 toast
    case "SHOW_TOAST":
      const data = action.payload as IToastPayload;
      state.toastData.toastMessage = data.toastMessage;
      state.toastData.toastType = data.toastType;
      state.toastData.toastShow = true;
      break;
    case "HIDE_TOAST":
      state.toastData.toastShow = false;
      break;
  }
  return state;
};

const store = createStore(reducer);

export default store;
