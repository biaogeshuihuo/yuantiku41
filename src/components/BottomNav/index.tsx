import React, { SyntheticEvent } from "react";
import BottomNavigation from "@mui/material/BottomNavigation";
import BottomNavigationAction from "@mui/material/BottomNavigationAction";
import "./bottomnav.less";
import { useLocation, useNavigate } from "react-router-dom";
const fast = require("@/assets/images/tabbar/fast.png");
const home_active = require("@/assets/images/tabbar/home_1.png");
const home = require("@/assets/images/tabbar/home_2.png");
const mine_active = require("@/assets/images/tabbar/my_1.png");
const mine = require("@/assets/images/tabbar/my_2.png");

const navs = [
  {
    path: "/home",
    text: "首页",
    icon: home,
    active: home_active,
  },
  {
    path: "/fast",
    text: "快速刷题",
    icon: fast,
    active: fast,
  },
  {
    path: "/mine",
    text: "我的",
    icon: mine,
    active: mine_active,
  },
];

export default function BottomNav() {
  // 获取路由路径
  const { pathname } = useLocation();
  const index = navs.findIndex((item) => item.path === pathname);
  const [value, setValue] = React.useState(index);
  const navigate = useNavigate();

  const changeNav = (event: SyntheticEvent, newValue: number) => {
    setValue(newValue);
    navigate(navs[newValue].path);
  };
  return (
    <div className="bot-nav" style={{ display: index >= 0 ? "block" : "none" }}>
      <BottomNavigation showLabels value={value} onChange={changeNav}>
        {navs.map((item, index) => (
          <BottomNavigationAction
            key={index}
            label={item.text}
            icon={<img src={value === index ? item.active : item.icon} />}
          />
        ))}
      </BottomNavigation>
    </div>
  );
}
