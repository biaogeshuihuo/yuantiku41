import React from "react";
import { useLocation, useNavigate } from "react-router-dom";
import "./backprev.less";

const navs = ["/home", "/fast", "/mine"];

export default function BackPrev() {
  const navigate = useNavigate();
  const { pathname } = useLocation();
  return (
    <div
      onClick={() => navigate(-1)}
      className="back-prev"
      style={{ display: navs.includes(pathname) ? "none" : "flex" }}
    >
      返回上一页
    </div>
  );
}
