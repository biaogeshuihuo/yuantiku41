import React, { useEffect } from "react";
import Alert from "@mui/material/Alert";
import { useDispatch, useSelector } from "react-redux";
import "./toast.less";
// 弹出的定时器
let timer: NodeJS.Timeout = setTimeout(() => {});
export default function Toast() {
  // 分析需求： 知道 有 几个数据要从外面传入： severity - 提示类型 ， msg - 提示信息 ， show - 是否显示
  // 但是这个组件我们放在 index.tsx 里面了，是全局入口的位置，在调用toast的组件里面想传递过来，太麻烦了
  // 所以把上面的三个数据放到 store 里面
  // 从store里面获取三个数据
  const { toastShow, toastMessage, toastType } = useSelector(
    (state: IRootState) => state.toastData
  );

  const dispatch = useDispatch();
  // 让 toast 自动隐藏
  // 当 show 从false 变成true，就让等待 2s 后隐藏
  // useEffect(() => {
  if (toastShow) {
    // 先把之前的定时器清除
    clearTimeout(timer);
    timer = setTimeout(() => {
      dispatch({ type: "HIDE_TOAST" });
    }, 2000);
  }
  // }, [toastShow]);
  return (
    <div className="toast" style={{ display: toastShow ? "block" : "none" }}>
      <Alert severity={toastType}>{toastMessage}</Alert>
    </div>
  );
}
