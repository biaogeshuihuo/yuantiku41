import React from "react";
import "./loading.less";
import styles from "./loading.module.less";

export default function Loading() {
  return (
    <div>
      <div className="loading"></div>
      {/* <div className={styles["loading-icon"]}>
        <div className={styles["loading-outside"]}>loading</div>
      </div> */}
    </div>
  );
}
